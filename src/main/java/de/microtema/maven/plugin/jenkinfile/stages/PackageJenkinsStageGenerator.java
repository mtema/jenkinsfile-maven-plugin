package de.microtema.maven.plugin.jenkinfile.stages;

import de.microtema.maven.plugin.jenkinfile.JenkinsfileData;

import static de.microtema.maven.plugin.jenkinfile.JenkinsfileGeneratorUtil.existsDockerfile;
import static de.microtema.maven.plugin.jenkinfile.JenkinsfileGeneratorUtil.getTemplate;

public class PackageJenkinsStageGenerator implements JenkinsStageGenerator {

    @Override
    public String execute(JenkinsfileData data) {

        if (existsDockerfile(data.getProject())) {
            return null;
        }

        return getTemplate("package");
    }
}
