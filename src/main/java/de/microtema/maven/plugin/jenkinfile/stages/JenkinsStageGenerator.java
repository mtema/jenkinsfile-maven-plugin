package de.microtema.maven.plugin.jenkinfile.stages;

import de.microtema.maven.plugin.jenkinfile.JenkinsfileData;

public interface JenkinsStageGenerator {

    String execute(JenkinsfileData data);
}
