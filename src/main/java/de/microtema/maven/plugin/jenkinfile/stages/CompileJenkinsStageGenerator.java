package de.microtema.maven.plugin.jenkinfile.stages;

import de.microtema.maven.plugin.jenkinfile.JenkinsfileData;

import static de.microtema.maven.plugin.jenkinfile.JenkinsfileGeneratorUtil.getTemplate;

public class CompileJenkinsStageGenerator implements JenkinsStageGenerator {

    @Override
    public String execute(JenkinsfileData mojo) {

        String template = getTemplate("compile");

        return template;
    }
}
