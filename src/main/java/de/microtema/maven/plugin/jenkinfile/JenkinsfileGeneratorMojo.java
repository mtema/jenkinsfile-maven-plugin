package de.microtema.maven.plugin.jenkinfile;

import de.microtema.maven.plugin.jenkinfile.stages.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.PrintWriter;
import java.util.*;

import static de.microtema.maven.plugin.jenkinfile.JenkinsfileGeneratorUtil.*;

@Mojo(name = "generate", defaultPhase = LifecyclePhase.COMPILE)
public class JenkinsfileGeneratorMojo extends AbstractMojo implements JenkinsfileData {

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    MavenProject project;

    @Parameter(property = "upstream-projects")
    String[] upstreamProjects = new String[0];

    @Parameter(property = "base-namespace")
    String baseNamespace;

    @Parameter(property = "bootstrap-url")
    String bootstrapUrl;

    @Parameter(property = "app-name")
    String appName;

    @Parameter(property = "environments")
    LinkedHashMap<String, String> environments = new LinkedHashMap<>();

    @Parameter(property = "stages")
    LinkedHashMap<String, String> stages = new LinkedHashMap<>();

    List<JenkinsStageGenerator> stageGenerators = new ArrayList<>();

    public void execute() {

        stageGenerators.add(new InitializeJenkinsStageGenerator());
        stageGenerators.add(new VersioningJenkinsStageGenerator());
        stageGenerators.add(new CompileJenkinsStageGenerator());
        stageGenerators.add(new PackageJenkinsStageGenerator());

        // Skip maven sub modules
        if (!isGitRepo(project)) {

            logMessage("Skip maven module: " + appName + " since it is not a git repo!");

            return;
        }

        String rootPath = getRootPath(project);

        logMessage("Generate Jenkinsfile for " + appName + " -> " + rootPath);

        String agent = getTemplate("agent");
        String environment = getEnvironmentTemplate();
        String options = getTemplate("options");
        String triggers = buildTriggers();
        String stagesTemplate = buildStages();
        String pipeline = getTemplate("pipeline");

        pipeline = pipeline.replace("@AGENT@", agent)
                .replace("@ENVIRONMENT@", environment)
                .replace("@OPTIONS@", options)
                .replace("@TRIGGERS@", triggers)
                .replace("@STAGES@", stagesTemplate);

        try (PrintWriter out = new PrintWriter(rootPath + "/Jenkinsfile")) {
            out.println(pipeline);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private String getEnvironmentTemplate() {

        String template = getTemplate("environment");

        StringBuilder environmentsAsString = new StringBuilder();

        environments.putIfAbsent("APP_NAME", appName);

        for (Map.Entry<String, String> entry : environments.entrySet()) {

            String value = entry.getValue();

            if (!(value.startsWith("sh") || value.startsWith("'") || value.startsWith("\""))) {
                value = maskEnvironmentVariable(value);
            }

            environmentsAsString.append(entry.getKey()).append(" = ").append(value).append("\n");
        }

        return template.replace("@ENVIRONMENTS@", paddingLine(environmentsAsString.toString(), 8));
    }

    String buildStages() {

        StringBuilder body = new StringBuilder();

        for (JenkinsStageGenerator stageGenerator : stageGenerators) {

            String stageTemplate = stageGenerator.execute(this);

            if (StringUtils.isNotEmpty(stageTemplate)) {
                body.append("\n");
                body.append(paddingLine(stageTemplate, 8));
                body.append("\n");
            }
        }

        return body.toString();
    }

    String buildTriggers() {

        String template = getTemplate("triggers");

        StringBuilder upstreamProjectsParam = new StringBuilder();

        for (int index = 0; index < upstreamProjects.length; index++) {

            String upstreamProject = upstreamProjects[index];

            upstreamProjectsParam.append(upstreamProject);
            upstreamProjectsParam.append("/${env.BRANCH_NAME.replaceAll('/', '%2F')}");

            if (index < upstreamProjects.length - 1) {
                upstreamProjectsParam.append(",");
            }
        }

        return template.replace("@UPSTREAM_PROJECTS@", upstreamProjectsParam.toString());
    }

    void logMessage(String message) {
        getLog().info("+----------------------------------+");
        getLog().info(message);
        getLog().info("+----------------------------------+");
    }

    @Override
    public String getBootstrapUrl() {
        return bootstrapUrl;
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public List<String> getSupportedBranches() {

        List<String> branches = new ArrayList<String>();

        for (Map.Entry<String, String> stage : stages.entrySet()) {

            branches.addAll(Arrays.asList(stage.getValue().split(",")));
        }

        return branches;
    }
}
