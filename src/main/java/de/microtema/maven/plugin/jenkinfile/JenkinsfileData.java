package de.microtema.maven.plugin.jenkinfile;

import org.apache.maven.project.MavenProject;

import java.util.List;

public interface JenkinsfileData {

    String getBootstrapUrl();

    MavenProject getProject();

    List<String> getSupportedBranches();
}
