package de.microtema.maven.plugin.jenkinfile;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.project.MavenProject;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class JenkinsfileGeneratorUtil {

    public static String getTemplate(String templateName) {

        InputStream inputStream = JenkinsfileGeneratorUtil.class.getResourceAsStream("/" + templateName + ".Jenkinsfile");

        try {
            return IOUtils.toString(inputStream, Charset.defaultCharset());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    public static String maskEnvironmentVariable(String value) {

        return "'" + StringUtils.trimToEmpty(value) + "'";
    }

    public static String paddingLine(String template, int padding) {

        StringBuilder stringBuilder = new StringBuilder();
        List<String> spaces = new ArrayList<>();

        while (padding-- > 0) {
            spaces.add(" ");
        }

        String paddingString = String.join("", spaces);

        try (BufferedReader reader = new BufferedReader(new StringReader(template))) {
            String line;
            int count = 0;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(count > 0 ? "\n" : "").append(paddingString).append(line);
                count++;
            }

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        return stringBuilder.toString();
    }

    public static String applyBranches(String template, List<String> supportedBranches) {

        StringBuilder branches = new StringBuilder();

        int count = 0;
        for (String branch : supportedBranches) {
            branches.append((count++ > 0) ? "\n" : "").append("branch").append(" ").append(maskEnvironmentVariable(branch));
        }

        String line = paddingLine(branches.toString(), 12);

        return template.replace("@BRANCHES@", line);
    }

    public static String getRootPath(MavenProject project) {

        return project.getBasedir().getPath();
    }

    public static boolean existsDockerfile(MavenProject project) {

        return new File(getRootPath(project) + "/Dockerfile").exists();
    }

    public static boolean existsDbMigrationScripts(MavenProject project) {

        return new File(getRootPath(project), "src/main/resources/db/migration").exists();
    }

    public static boolean hasSourceCode(MavenProject project) {

        if (new File(getRootPath(project), "src/main/java").exists()) {
            return true;
        }

        return project.getModules().stream().noneMatch(it -> ((String) it).startsWith("../"));
    }

    public static boolean isGitRepo(MavenProject project) {

        return new File(getRootPath(project), ".git").exists();
    }

    public static List<String> getSonarExcludes(MavenProject project) {

        List<String> excludes = new ArrayList<>(project.getModules());

        excludes.removeIf(it -> it.startsWith("../"));
        excludes.removeIf(it -> new File(new File(getRootPath(project), it), "src/main/java").exists());

        return excludes;
    }

    public static boolean hasSonarProperties(MavenProject project) {

        long count = project.getProperties().entrySet()
                .stream()
                .filter(it -> String.valueOf(it.getKey()).startsWith("sonar.")).count();

        return count > 0;
    }

    public static boolean existsUnitTests(MavenProject project) {
        String rootPath = getRootPath(project);

        File rootDir = new File(rootPath, "src/test/java");

        return findFile(rootDir.toPath(), "Test.java");
    }

    public static boolean existsIntegrationTests(MavenProject project) {
        String rootPath = getRootPath(project);

        File rootDir = new File(rootPath, "src/test/java");

        return findFile(rootDir.toPath(), "IT.java");
    }

    static boolean findFile(Path targetDir, String fileName) {
        try {
            return Files.list(targetDir).anyMatch((p) -> {
                if (Files.isRegularFile(p)) {
                    String file = p.getFileName().toString();
                    return file.contains(fileName);
                } else {
                    return findFile(p, fileName);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
