package de.microtema.maven.plugin.jenkinfile.stages;

import de.microtema.maven.plugin.jenkinfile.JenkinsfileData;

import java.util.List;

import static de.microtema.maven.plugin.jenkinfile.JenkinsfileGeneratorUtil.*;

public class VersioningJenkinsStageGenerator implements JenkinsStageGenerator {

    @Override
    public String execute(JenkinsfileData data) {

        if (existsDockerfile(data.getProject())) {
            return null;
        }

        List<String> branches = data.getSupportedBranches();

        if (branches.isEmpty()) {
            return null;
        }

        String template = getTemplate("versioning");

        return applyBranches(template, branches);
    }
}
