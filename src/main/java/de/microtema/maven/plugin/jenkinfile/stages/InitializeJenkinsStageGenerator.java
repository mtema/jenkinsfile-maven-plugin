package de.microtema.maven.plugin.jenkinfile.stages;

import de.microtema.maven.plugin.jenkinfile.JenkinsfileData;
import org.apache.commons.lang3.StringUtils;

import static de.microtema.maven.plugin.jenkinfile.JenkinsfileGeneratorUtil.getTemplate;
import static de.microtema.maven.plugin.jenkinfile.JenkinsfileGeneratorUtil.maskEnvironmentVariable;

public class InitializeJenkinsStageGenerator implements JenkinsStageGenerator {

    @Override
    public String execute(JenkinsfileData data) {

        String template = getTemplate("initialize");
        String bootstrap = StringUtils.EMPTY;
        String bootstrapUrl = data.getBootstrapUrl();

        if (StringUtils.isNotEmpty(bootstrapUrl)) {
            bootstrap = getTemplate("initialize-bootstrap");
        }

        return template
                .replace("@BOOTSTRAP_URL@", maskEnvironmentVariable(bootstrapUrl))
                .replace("@BOOTSTRAP@", bootstrap);
    }
}
