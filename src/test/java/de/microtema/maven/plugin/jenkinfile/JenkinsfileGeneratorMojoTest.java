package de.microtema.maven.plugin.jenkinfile;

import org.apache.commons.io.FileUtils;
import org.apache.maven.project.MavenProject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.util.LinkedHashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JenkinsfileGeneratorMojoTest {

    @InjectMocks
    JenkinsfileGeneratorMojo sut;

    @Mock
    MavenProject project;

    @Mock
    File basePath;

    File jenkinsfile = new File("./Jenkinsfile");

    @BeforeEach
    void setUp() {

        sut.project = project;
        sut.appName = "app";
        sut.baseNamespace = "ns";
        sut.bootstrapUrl = "http://bootstrap.git";
        sut.stages = new LinkedHashMap<>();
        sut.stages.put("DEV", "develop");
        sut.stages.put("QA", "release/*,bugfix/*");
        sut.stages.put("PRD", "master");
    }

    @AfterEach
    void tearDown() {

        jenkinsfile.delete();
    }

    @Test
    void executeOnNonUpdateFalse() throws Exception {

        when(project.getBasedir()).thenReturn(basePath);
        when(basePath.getPath()).thenReturn(".");

        sut.execute();

        String answer = FileUtils.readFileToString(jenkinsfile, "UTF-8");

        assertEquals("pipeline {\n" +
                "\n" +
                "    agent {\n" +
                "        label 'mvn8'\n" +
                "    }\n" +
                "\n" +
                "    environment {\n" +
                "        CURRENT_TIME = sh(script: 'date +%Y-%m-%d-%H-%M', returnStdout: true).trim()\n" +
                "        CHANGE_AUTHOR_EMAIL = sh(script: \"git --no-pager show -s --format='%ae'\", returnStdout: true).trim()\n" +
                "        APP_NAME = 'app'\n" +
                "    }\n" +
                "\n" +
                "    options {\n" +
                "        disableConcurrentBuilds()\n" +
                "        timeout(time: 30, unit: 'MINUTES')\n" +
                "        buildDiscarder(logRotator(numToKeepStr: '20', artifactNumToKeepStr: '10'))\n" +
                "    }\n" +
                "\n" +
                "    triggers {\n" +
                "        upstream(upstreamProjects: \"\", threshold: hudson.model.Result.SUCCESS)\n" +
                "    }\n" +
                "\n" +
                "    stages {\n" +
                "        \n" +
                "        stage('Initialize') {\n" +
                "        \n" +
                "            environment {\n" +
                "                BOOTSTRAP_URL = 'http://bootstrap.git'\n" +
                "            }\n" +
                "        \n" +
                "            steps {\n" +
                "                script {\n" +
                "                    if (env.BOOTSTRAP_URL.toLowerCase() == env.GIT_URL.toLowerCase()) {\n" +
                "                        env.MAVEN_ARGS = '-s ./settings.xml'\n" +
                "                    } else {\n" +
                "                        dir('bootstrap') {\n" +
                "                            try {\n" +
                "                                git branch: env.BRANCH_NAME, url: env.BOOTSTRAP_URL, credentialsId: 'SCM_CREDENTIALS'\n" +
                "                            } catch (e) {\n" +
                "                                git branch: 'develop', url: env.BOOTSTRAP_URL, credentialsId: 'SCM_CREDENTIALS'\n" +
                "                            }\n" +
                "                            env.MAVEN_ARGS = '-s ./bootstrap/settings.xml'\n" +
                "                        }\n" +
                "                    }\n" +
                "                }\n" +
                "        \n" +
                "                sh 'whoami'\n" +
                "                sh 'oc whoami'\n" +
                "                sh 'mvn -version'\n" +
                "                sh 'echo commit-id: $GIT_COMMIT'\n" +
                "                sh 'echo change author: $CHANGE_AUTHOR_EMAIL'\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        stage('Versioning') {\n" +
                "        \n" +
                "            environment {\n" +
                "                VERSION = sh(script: 'mvn help:evaluate -Dexpression=project.version -q -DforceStdout $MAVEN_ARGS', returnStdout: true).trim().replace('-SNAPSHOT', '')\n" +
                "            }\n" +
                "        \n" +
                "            when {\n" +
                "                anyOf {\n" +
                "                    branch 'develop'\n" +
                "                    branch 'release/*'\n" +
                "                    branch 'bugfix/*'\n" +
                "                    branch 'master'\n" +
                "                }\n" +
                "            }\n" +
                "        \n" +
                "            steps {\n" +
                "        \n" +
                "                script {\n" +
                "        \n" +
                "                    sh 'mvn release:update-versions -DdevelopmentVersion=2.1.0-SNAPSHOT $MAVEN_ARGS'\n" +
                "        \n" +
                "                    if(env.BRANCH_NAME == 'master') {\n" +
                "                       sh 'mvn versions:set -DnewVersion=$VERSION $MAVEN_ARGS'\n" +
                "                    } else {\n" +
                "                       sh 'mvn versions:set -DnewVersion=$VERSION-$CURRENT_TIME-$BUILD_NUMBER $MAVEN_ARGS'\n" +
                "                    }\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        stage('Compile') {\n" +
                "            steps {\n" +
                "                sh 'mvn compile -U $MAVEN_ARGS'\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        stage('Package') {\n" +
                "            steps {\n" +
                "               sh 'mvn package -Dmaven.test.skip=true -DskipTests=true -P prod $MAVEN_ARGS'\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "    }\n" +
                "\n" +
                "    post {\n" +
                "\n" +
                "        always {\n" +
                "            script {\n" +
                "                if (currentBuild.result == null) {\n" +
                "                    currentBuild.result = 'SUCCESS'\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        failure {\n" +
                "            mail subject: \"FAILED: Job '${env.JOB_NAME} in Branch ${env.BRANCH_NAME} [${env.BUILD_NUMBER}]'\",\n" +
                "                    body: \"FAILED: Job '${env.JOB_NAME} in Branch ${env.BRANCH_NAME} [${env.BUILD_NUMBER}]': Check console output at ${env.BUILD_URL}\",\n" +
                "                      to:  sh(script: \"git --no-pager show -s --format='%ae'\", returnStdout: true).trim()\n" +
                "        }\n" +
                "    }\n" +
                "}\n" +
                "\n", answer);
    }
}
